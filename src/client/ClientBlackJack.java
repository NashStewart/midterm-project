package client;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import blackjack_contract.Message;

public class ClientBlackJack 
{
	private static ObjectInputStream input;
	private static ObjectOutputStream output;
	private String username;


	// CONSTRUCTOR
	public ClientBlackJack(String ipAddress) throws IOException 
	{
		username = "Nash";
		GraphicalChat graphicalChat = new GraphicalChat(this);

		Socket connection = new Socket(InetAddress.getByName(ipAddress), 8989);

		output = new ObjectOutputStream(connection.getOutputStream());
		output.flush();

		output.writeUTF(username);
		output.flush();

		start(username);

		input = new ObjectInputStream(new BufferedInputStream(connection.getInputStream()));

		do{
			try {
				String message = input.readObject().toString();
				switch(message)
				{
				case "ACKNOWLEDGE":
					graphicalChat.appendDisplayText("Status: " + message);
					break;
				case "DENY":
					graphicalChat.appendDisplayText("Status: " + message);
					break;
				case "CHAT":
					graphicalChat.appendDisplayText("Chat - " + message);
					break;
				case "START_GAME":
					graphicalChat.appendDisplayText("Start - " + message);
				case "END_GAME":
					graphicalChat.appendDisplayText("End - " + message);
				case "ACTION":
					graphicalChat.appendDisplayText("Action - " + message);
				case "CARD":
					graphicalChat.appendDisplayText("Card - " + message);
				default:
					graphicalChat.appendDisplayText(message);
					break;
				}

			} catch(ClassNotFoundException classNotFoundException) {
				System.out.println("ERROR!!!");
			}
		} while(connection.isConnected() && !connection.isClosed());
		connection.close();
	}


	// Send Message
	public void sendMessage(String message) 
	{
		try {
			output.writeObject(Message.createChatMessage(message, "NashStewart"));
			output.flush();
		} catch(IOException ioException) {
			ioException.printStackTrace();
		}
	}


	// Hit
	public void hit() 
	{
		try {
			output.writeObject(Message.createActionMessage(Message.Action.HIT));
			output.flush();
		} catch(IOException ioException) {
			ioException.printStackTrace();
		}
	}


	// Stay
	public void stay() 
	{
		try {
			output.writeObject(Message.createActionMessage(Message.Action.STAY));
			output.flush();
		} catch(IOException ioException) {
			ioException.printStackTrace();
		}
	}


	// Start
	public void start(String username) 
	{
		try {
			output.writeObject(Message.createStartMessage(username));
			output.flush();
		} catch(IOException ioException) {
			ioException.printStackTrace();
		}
	}


	// End
	public void end() 
	{
		try {
			output.writeObject(Message.createEndMessage(username));
			output.flush();
		} catch(IOException ioException) {
			ioException.printStackTrace();
		}
	}


	public static void main(String[] args) throws IOException 
	{	
		ClientBlackJack game = new ClientBlackJack("127.0.0.1");
		game.sendMessage("Nash is connected!");
	}
}
