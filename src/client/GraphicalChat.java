package client;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.text.DefaultCaret;

public class GraphicalChat extends JFrame
{
	private static final long serialVersionUID = -7116132329777144721L;
	private JTextArea textAreaDisplay;
	private ClientBlackJack chatClient;
	private JLabel lblServerCards;
	private JLabel lblClientCards;
	private int serverScore;
	private int clientScore;


	// CONSTRUCOR
	public GraphicalChat(ClientBlackJack chatClient) 
	{	
		this.chatClient = chatClient;
		setLayout(new BorderLayout());
		createPanelChat();

		setTitle("Graphical Chat - Nash Stewart");
		addWindowListener(new WindowAdapter() 
		{
			public void windowClosing(WindowEvent e) 
			{
				chatClient.end();
				System.exit(0);
			}
		});
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(1000, 600);
		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
	}

	public GraphicalChat() 
	{	
		serverScore = 0;
		clientScore = 0;
		setLayout(new BorderLayout());
		createPanelChat();


		setTitle("Graphical Chat - Nash Stewart");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(800, 600);
		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
	}


	// Create chat panel
	private void createPanelChat() 
	{
		JPanel panelMain = new JPanel();
		panelMain.setLayout(new FlowLayout());

		textAreaDisplay = new JTextArea(30, 38);
		textAreaDisplay.setLineWrap(true);
		textAreaDisplay.setEditable(false);
		JScrollPane scrollPaneDisplay = new JScrollPane(textAreaDisplay);
		DefaultCaret caret = (DefaultCaret)textAreaDisplay.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

		JTextArea textAreaInput = new JTextArea(2, 38);
		textAreaInput.setLineWrap(true);
		textAreaInput.setToolTipText("ctrl + enter to send");
		JScrollPane scrollPaneInput = new JScrollPane(textAreaInput);

		JButton btnSend = new JButton("Send");
		btnSend.addMouseListener(new MouseAdapter() 
		{ 
			public void mousePressed(MouseEvent me) 
			{ 
				String message = textAreaInput.getText();
				appendDisplayText("You: " + message);
				chatClient.sendMessage(message);
				textAreaInput.setText("");
				textAreaInput.requestFocus();
			} 
		});
		textAreaInput.addKeyListener(new KeyAdapter() 
		{
			public void keyPressed(KeyEvent e) 
			{                
				if((e.getKeyCode() == KeyEvent.VK_ENTER) && ((e.getModifiers() & KeyEvent.CTRL_MASK) != 0))
				{
					String message = textAreaInput.getText();
					appendDisplayText("You: " + message + " (ctlr+enter)");
					chatClient.sendMessage(message);
					textAreaInput.setText("");
				}
			}        
		});

		panelMain.add(scrollPaneDisplay);
		panelMain.add(scrollPaneInput);
		panelMain.add(btnSend);
		add(panelMain, BorderLayout.EAST);
	}


	// Create game panel
	private void createGamePanel() 
	{
		lblServerCards = new JLabel("0");
		lblClientCards = new JLabel("0");

		JButton btnHit = new JButton("Hit");
		btnHit.addKeyListener(new KeyAdapter() 
		{
			public void keyPressed(KeyEvent e) 
			{                

			}        
		});

		JButton btnStay = new JButton("Stay");
		btnStay.addKeyListener(new KeyAdapter() 
		{
			public void keyPressed(KeyEvent e) 
			{                

			}        
		});

		JButton btnStart = new JButton("New Game");
		btnStart.addKeyListener(new KeyAdapter() 
		{
			public void keyPressed(KeyEvent e) 
			{                

			}        
		});
	}


	public void appendDisplayText(String str)
	{
		textAreaDisplay.append(str + "\n");
	}


	public static void main(String args[])
	{
		GraphicalChat chat = new GraphicalChat();
		chat.appendDisplayText("This is a test run.");
	}
}
